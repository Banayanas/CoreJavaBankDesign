package bankProjectwithchanges;

public abstract class bankAccount {

	public double balance;
	public String sortCode;
	public String accNumber;
	public String idNumber;
	

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getSortCode() {
		return sortCode;
	}

	public void setSortCode(String sortCode) {

		if (sortCode.length() == 6 && sortCode.matches("[0-9]+"))
		{
			this.sortCode = sortCode;
		}
		else
		{
			System.out.println("Sort code must be 6 digits");
		}
	}


	public String getAccNumber() {
		return accNumber;
	}

	public void setAccNumber(String accNumber) {
		if (accNumber.length() == 8) {
			this.accNumber = accNumber;
		}
		else {
			System.out.println("Account number must be 8 digits.");
		}

	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		if (idNumber.length() <= 0) {
			System.out.println("ID number must be at least one character.");
		}
		else {
			this.idNumber = idNumber;
		}

	}


	//Constructor to set the attributes to the values passed in by the Setters
	public bankAccount(double balance, String sortCode, String accNumber, String idNumber)
	{
		this.setBalance(balance);
		this.setSortCode(sortCode);
		this.setAccNumber(accNumber);
		this.setIdNumber(idNumber);
	}


	protected void deposit(double depositAmount)
	{
		if(depositAmount < 0)
		{
			System.out.println("Deposit amount must be a positive value");
		}
		else
		{
			balance = balance + depositAmount;
		}
	}

	protected void withdraw(double withdrawAmount)
	{
		if(withdrawAmount < 0)
		{
			System.out.println("Withdraw amount must be a positive value");
		}
		else
		{
			balance = balance + withdrawAmount;
		}
	}

}
