package bankProjectwithchanges;

public class currentAccount extends bankAccount implements everydayAccounts {

	private double overdraftLimit = 0;
	private double overdraftAmount;
	private double overdraftInterestRate = 12.4;

	//setters with validation

	public void setOverdraftLimit(double overdraftLimit) {
		if(overdraftLimit >= 0 && overdraftLimit <= overdraftLimit) {
			this.overdraftLimit = overdraftLimit;
		}
		else if (overdraftLimit < 0){
			System.out.println("When having an overdraft, the limit must be a positive value. Current value is: " + overdraftLimit);
		}
		else if (overdraftLimit > 2500) {
			System.out.println("When having an overdraft, the limit or amount should not exceed $2500.00");
		}
		else {
			System.out.println("Invalid overdraft limit.");
		}

	}
	private void setOverdraftAmount(double overdraftAmount) {
		if (overdraftAmount <= overdraftLimit) {
			this.overdraftAmount = overdraftAmount;
		}
		else if (overdraftAmount < 0) {
			System.out.println("When having an overdraft, the amount must be a positive value. Current value is: " + overdraftAmount);
		}
		else if (overdraftAmount > overdraftLimit){
			System.out.println("not allowed to have overdraft higher than " + String.format("%.2f",  overdraftLimit) + ".");
		}
		else {
			System.out.println("Invalid overdraft amount.");
		}
	}

	public void setOverdraftInterestRate(double overdraftInterestRate) {
		if (overdraftInterestRate >= 0 && overdraftInterestRate <= 100) {
			this.overdraftInterestRate = overdraftInterestRate;
		}
		else {
			System.out.println("Interest rate is requires to be between 0% and 100% for current account.");
		}
	}

	//getters
	public double getOverdraftLimit() {
		return overdraftLimit;
	}
	public double getOverdraftAmount() {
		return overdraftAmount;
	}
	public double getOverdraftInterestRate() {
		return overdraftInterestRate;
	}

	//Default Constructor 1
	public currentAccount(double accountBalance, String accountSortCode, String accountNumber, String accountId)
	{
		super(accountBalance, accountSortCode, accountNumber, accountId);
		this.setOverdraftLimit(overdraftLimit);
		this.setOverdraftAmount(overdraftAmount);
		this.setOverdraftInterestRate(overdraftInterestRate);
	}

	//Constructor 2 With Overdraft
	public currentAccount(double accountBalance, String accountSortCode, String accountNumber, String accountId,  double overdraftLimit, double overdraftAmount, double overdraftInterest)
	{
		super(accountBalance, accountSortCode, accountNumber, accountId);
		this.setOverdraftLimit(overdraftLimit);
		this.setOverdraftAmount(overdraftAmount);
		this.setOverdraftInterestRate(overdraftInterest);
	}
	public double updateBalanceInterestRate() {
		this.setBalance(this.getBalance() + (this.getBalance() * (this.getOverdraftInterestRate() / 100.00 )));
		return balance;
	}
	@Override
	protected void withdraw(double withdrawAmount) {
		if(withdrawAmount <= balance) {
			balance -= withdrawAmount;
		}
		else {
			System.out.println("\n\nCan only withdraw up to $" + (balance + overdraftLimit) + "\n\n");
		}
	}
	public String toString() {
		return("\nBank account balance: " + balance  + "\nSort code: " + sortCode + "\nAccount number: " + accNumber + "\nAccount number ID: " + idNumber + "\nOverdraft amount: " + overdraftAmount + "\nOverdraft limit: " + overdraftAmount + "\nOverdraft interest rate: " + overdraftInterestRate + "\n");
	}
}
