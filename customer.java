package bankProjectwithchanges;

import java.util.Random;
import java.util.ArrayList;
import java.util.List;

public class customer {
	//array list to hold customer information
	private String firstName;
	private String lastName;
	private String id;
	boolean auto = false;
	
	Random random = new Random();
	int idRandom = random.nextInt(100);
	
	public List<bankAccount> accounts;

	//setters

	public void setFirstName(String firstName) {
		if(firstName.length() > 0 && firstName.matches("[a-zA-Z]*")) {	
			this.firstName = firstName;
		}
	}

	public void setLastName(String lastName) {
		if (lastName.length() > 0 && firstName.matches("[a-zA-Z]*")) {
			this.lastName = lastName;
		}
	}

	public void setId(String id) {
			this.id = id;
		}

	//getters

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getId() {
		return id;
	}

	//default constructor //Customer auto ID
	public customer(String firstName, String lastName)
	{
		auto = true;
		this.setFirstName(firstName);
		this.setLastName(lastName);
	
	}
	//defining ID
	public customer(String firstName, String lastName, String id) {
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setId(id);
	}
	
	
	public void addAccount(bankAccount account) {
		if(accounts == null) {
			accounts = new ArrayList<bankAccount>();
			accounts.add(account);
		}
		else if (accounts == accounts) {
			System.out.println("Cannot add identical account.");
		}
	}

	public void removeAccount(bankAccount account) {
		if(accounts.contains(account)) {
			accounts.remove(account);
		}
		else if(accounts.isEmpty()) {
			accounts = null;
			System.out.println("Customer has no accounts.");
		}
		else {
			System.out.println("Account not found.");
		}
	}

	public String toString() {
		if (this.accounts == null && auto == false) {
			String accNone = "Has no accounts";
			return("\nCustomer ID: " + id + "\nFirst name: " + firstName + "\nLast name: " + lastName + "\n" + accNone);
		}
		else if (auto == false) {
			return("\nCustomer ID: " + id + "\nFirst name: " + firstName + "\nLast name: " + lastName + "\n\n" + accounts);
		}
		else if (this.accounts == null && auto == true) {
			String accNone = "Has no accounts";
			return("\nCustomer ID: " + idRandom + "\nFirst name: " + firstName + "\nLast name: " + lastName + "\n" + accNone);
		}
		else if (auto == true) {
			return("\nCustomer ID: " + idRandom + "\nFirst name: " + firstName + "\nLast name: " + lastName + "\n\n" + accounts);
		}
		return  "d";
		
	}
	
}


