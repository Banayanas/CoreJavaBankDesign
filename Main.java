package bankProjectwithchanges;
import java.util.Scanner;
public class Main{

	public static void main(String[] args) {
		
		boolean authentication = false;
		//authentication being true allows for one test execution (iteration of loop)
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the administrative password:");
		String password = sc.nextLine();
		
		if (password.matches("Password")) {
			authentication = true;
		}
		
		while (authentication == true) {
		//creating savers accounts
		//	savingsAccount sa1 = new savingsAccount(66.50, "123456", "21222324", "90", 10.0);
		//	savingsAccount sa2 = new savingsAccount(200.50, "234567", "41414141", "100", 12.0);
		//  savingsAccount sa3 = new savingsAccount(100.50, "909090", "90909090", "90", 10.0);
		//	System.out.println(sa1.toString()); //+ "\n" + sa2.toString());// + "\n" + sa3.toString());
		//	sa1.deposit(5000.4);
		//	sa2.deposit(4843.3);
		//	sa3.deposit(3423.4);
		//	System.out.println(sa1.toString());

		//	sa1.setBalance(100000.0);
		//	System.out.println(sa1.toString());

		//	sa1.withdraw(500);
		//	System.out.println(sa1.toString());

		//	sa1.updateBalanceInterestRate(6.5);
		//	System.out.println(sa1.toString());

		//creating Isas
		//isas i1 = new isas(100.50, "909090", "90909090", "90");
		//isa with interest
		//isas i1 = new isas(100.50, "909090", "90909090", "90", 12);
		//	isas i2 = new isas(100.50, "909090", "90909090", "90");
		//	isas i3 = new isas(100.50, "909090", "90909090", "90");
		//	System.out.println(i1.toString() + "\n" + i2.toString() + "\n" + i3.toString());
		//	i1.deposit(504540.3);
		//	i2.deposit(4324.3);
		//	i3.deposit(33243.4);
		//	i1.withdraw(1000);
		//i1.updateBalanceInterestRate(6.5);
		//System.out.println(i1.toString());

		//creating current accounts
			currentAccount ca1 = new currentAccount(10.4, "101010", "10101010", "10000", 100, 60.50, 15.5);
		//	currentAccount ca2 = new currentAccount(10.4, "101010", "10101010", "ds", 60.5, 100.50, 15.5);
		//	currentAccount ca3 = new currentAccount(10.4, "101010", "10101010", "ds", 60.5, 100.50, 15.5);
		//	System.out.println(ca1.toString() + "\n" + ca2.toString() + "\n" + ca3.toString());
		//	ca1.deposit(504540.3);
		//	ca2.deposit(4324.3);
		//	ca3.deposit(33243.4);
		//	ca2.withdraw(1000);
		//	System.out.println(ca1.toString());

		//creating customers
		
		//Write "Auto" in the customer ID field to automatically generate a unique ID
			//customer customer1 = new customer("Sam", "Coates", "10");
			customer customer1 = new customer("Sam", "Coates");
		//	customer customer2 = new customer("john", "lastname", "10");
		//	customer customer3 = new customer("kevin", "lastname", "10");
		//	customer customer4 = new customer("michael", "lastname", "10");
		//	customer customer5 = new customer("travis", "lastname", "10");
		//	customer customer6 = new customer("jordan", "lastname", "10");
		//	customer customer7 = new customer("v", "lastname", "10");
		//	customer customer8 = new customer("liv", "lastname", "10");
		//	customer customer9 = new customer("lewis", "lastname", "10");
		//	System.out.println(customer1.toString() + "\n" + customer2.toString() + "\n" + customer3.toString() + customer4.toString() + "\n" + customer5.toString() + "\n" + customer6.toString() + customer7.toString() + "\n" + customer8.toString() + "\n" + customer9.toString());
		//	customer1.addAccount(ca1);
		//	customer2.addAccount(ca3);
		//	customer3.addAccount(sa3);
		//	customer4.addAccount(i2);
		//	customer5.addAccount(ca2);
		//	customer6.addAccount(sa2);
		//	customer7.addAccount(i1);
		//	customer8.addAccount(ca1);
		// 	customer9.addAccount(sa1);
			//customer1.removeAccount(ca1);
		//	customer1.addAccount(sa2);
			customer1.addAccount(ca1);
			System.out.println(customer1.toString());

		//print out account information of all accounts owned by a customer
		//update interest value on personal saver accounts owner by a customer
		authentication = false;
		}
		sc.close();
	}
}