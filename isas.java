package bankProjectwithchanges;

public class isas extends bankAccount implements personalSaverAccount{
	//cannot have negative balance
	//no overdraft, just interest
	private double interestRate;

	//setters with validation


	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	//getters
	public double getInterestRate() {
		return interestRate;
	}

	//Default Constructor 1
	public isas(double accountBalance, String accountSortCode, String accountNumber, String accountId)
	{
		super(accountBalance, accountSortCode, accountNumber, accountId);
		this.setInterestRate(0.0);
	}

	//Constructor 2 With Interest
	public isas(double accountBalance, String accountSortCode, String accountNumber, String accountId, double interestRate)
	{
		super(accountBalance, accountSortCode, accountNumber, accountId);
		this.setInterestRate(interestRate);
	}
	public double updateBalanceInterestRate(double interestRate) {
		this.setBalance(this.getBalance() + (this.getBalance() * (this.getInterestRate() / 100.00 )));
		return balance;
	}
	@Override
	protected void withdraw(double withdrawAmount) {
		if(withdrawAmount <= balance) {
			balance -= withdrawAmount;
		}
		else {
			System.out.println("\n\nCan only withdraw up to $" + balance + "\n\n");
		}
	}
	public String toString() {
		return("\nBank account balance is " + balance  + "\nThe sort code is " + sortCode + "\nThe account number is " + accNumber + "\nThe account number id is " + idNumber + "\nThe interest rate is " + interestRate + "\n");
	}
}
